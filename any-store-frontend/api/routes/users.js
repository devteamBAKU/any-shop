const { Router } = require('express')
const router = Router()
const axios = require('axios')

const rootUrl = 'http://localhost:8000/api/'

// Mock Users
const users = [{ name: 'Alexandre' }, { name: 'Pooya' }, { name: 'Sébastien' }]

/* GET users listing. */
router.get('/users', function (req, res, next) {
  axios({
    method: 'GET',
    url: rootUrl.concat('users/')
  }).then((response) => {
    console.log('res -> ', response)
    if (response.status === 200) {
      return res.json(response.data)
    } else {
      return response.json('[]')
    }
  })
})

/* GET user by ID. */
router.get('/users/:id', function (req, res, next) {
  const id = req.params.id
  axios({
    method: 'GET',
    url: rootUrl.concat('users/').concat(id)
  }).then((response) => {
    if (response.status === 200) {
      return res.json(response.data)
    } else {
      return res.json([])
    }
  })
})

module.exports = router
