import axios from 'axios'
export const getUsers = () => {
  return axios({
    method: 'get',
    url: '/api/users/'
  })
    .then((res) => {
      console.log('RES -> ', res)
      return res.data
    })
    .catch((ex) => {
      console.log('ex -> ', ex)
      return ex
    })
}
